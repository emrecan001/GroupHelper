import logging
import sys
from os import environ
from telethon import TelegramClient
import telegram.ext as tg

#Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO)

LOGGER = logging.getLogger(__name__)

LOGGER.info("Starting group_helper...")

# If Python version is < 3.6, stops the bot.
if sys.version_info[0] < 3 or sys.version_info[1] < 8:
    LOGGER.error(
        "You MUST have a python version of at least 3.8! Multiple features depend on this. Bot quitting."
    )
    quit(1)

# Load config
TOKEN = environ.get('TOKEN', None)
try:
    OWNER_ID = int(environ.get('OWNER_ID', None))
except ValueError:
    raise Exception("Your OWNER_ID env variable is not a valid integer.")
MESSAGE_DUMP = environ.get('MESSAGE_DUMP', None)
OWNER_USERNAME = environ.get("OWNER_USERNAME", None)
API_KEY = environ.get('API_KEY', None)
API_HASH = environ.get('API_HASH', None)

try:
    SUDO_USERS = set(int(x)
                     for x in environ.get("SUDO_USERS", "").split())
except ValueError:
    raise Exception(
        "Your sudo users list does not contain valid integers.")
try:
    WHITELIST_USERS = set(int(x) for x in environ.get(
        "WHITELIST_USERS", "").split())
except ValueError:
    raise Exception(
        "Your whitelisted users list does not contain valid integers.")
WEBHOOK = bool(environ.get('WEBHOOK', False))
URL = environ.get('URL', "")  # Does not contain token
PORT = int(environ.get('PORT', 5000))
CERT_PATH = environ.get("CERT_PATH")
DB_URI = str(environ.get('DATABASE_URL'))
LOAD = environ.get("LOAD", "").split()
NO_LOAD = environ.get("NO_LOAD", "translation").split()
DEL_CMDS = bool(environ.get('DEL_CMDS', False))
STRICT_ANTISPAM = bool(environ.get('STRICT_GBAN', False))
WORKERS = int(environ.get('WORKERS', 8))
ALLOW_EXCL = environ.get('ALLOW_EXCL', False)
spamwatch_api = environ.get("SPAMWATCH_API", None)
SUDO_USERS.add(OWNER_ID)
# SpamWatch

if spamwatch_api == "None":
    spamwatch_api == None

updater = tg.Updater(TOKEN, workers=WORKERS)

dispatcher = updater.dispatcher

tbot = TelegramClient("grouphelper", API_KEY, API_HASH)

SUDO_USERS = list(SUDO_USERS)
WHITELIST_USERS = list(WHITELIST_USERS)

# Load at end to ensure all prev variables have been set
from group_helper.modules.helper_funcs.handlers import CustomCommandHandler, CustomRegexHandler

# make sure the regex handler can take extra kwargs
tg.RegexHandler = CustomRegexHandler

tg.CommandHandler = CustomCommandHandler
